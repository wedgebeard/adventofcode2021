using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Day06A : MonoBehaviour
{
    //answer: 377263 (80 days)
    public string file_path;
    private int daysToAge = 5;

    void Start()
    {
        Debug.Log("Day 06 A");
        IList<string> input_array = ReadTextFile(file_path);

        IList<int> lanternFishList = ConvertStringToIntList(input_array[0]);

        IList<int> agedLanternFishList = AgedLanternFishList(lanternFishList, daysToAge);

        Debug.Log("done: " + agedLanternFishList.Count);
    }

    private IList<int> AgedLanternFishList(IList<int> lanternFishList, int daysToAge)
    {
        for (int day = 0; day < daysToAge; day++)
        {
            int newFishSpawnCount = 0;
            for (int i = 0; i < lanternFishList.Count; i++)
            {
                lanternFishList[i] -= 1;
                if (lanternFishList[i] < 0)
                {
                    newFishSpawnCount++;
                    lanternFishList[i] = 6;
                }
            }
            for (int i = 0; i < newFishSpawnCount; i++)
            {
                lanternFishList.Add(8);
            }
        }

        return lanternFishList;
    }

    private IList<int> ConvertStringToIntList(string input_array)
    {
        IList<int> fishList = new List<int>();
        string[] separateFish = input_array.Split(',');

        for (int i = 0; i < separateFish.Length; i++)
        {
            fishList.Add(int.Parse(separateFish[i]));
        }

        return fishList;
    }

    IList<string> ReadTextFile(string file_path)
    {
        IList<string> input_array = new List<string>();

        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
        
        return input_array;
    }
}
