﻿using System.Collections;

namespace Assets.Scripts
{
    public class Node
    {
        public Node(int height, bool inBasin, int row, int col)
        {
            Height = height;
            InBasin = inBasin;
            Position = new Position(row, col);
        }

        public int Height { get; set; }
        public bool InBasin { get; set; }
        public Position Position { get; set; }
    }

    public class Position
    {
        public Position(int row, int col)
        {
            Row = row;
            Col = col;
        }

        public int Row { get; set; }
        public int Col{ get; set; }
    }
}

