using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day03B : MonoBehaviour
{
    //answer: 1032597
    public string file_path;

    void Start()
    {
        IList<string> input_list = new List<string>();
        ReadTextFile(file_path, input_list);
        string oxygenGeneratorRating = FindLifeSupportRating('1', input_list, 0, true);
        string co2ScrubberRating = FindLifeSupportRating('0', input_list, 0, false);

        Debug.Log("OGR: " + oxygenGeneratorRating + " | CO2S: " + co2ScrubberRating + " Life support rating: " +
            (Convert.ToInt32(oxygenGeneratorRating, 2) * (Convert.ToInt32(co2ScrubberRating, 2))));

    }

    private string FindLifeSupportRating(char primaryDigit, IList<string> input_list, int index, bool keepMost)
    {
        if (input_list.Count == 1) { return input_list[0]; }
        string finalBinaryNumber = "";
        IList<string> primary = new List<string>();
        IList<string> secondary = new List<string>();

        for (int i = 0; i < input_list.Count; i++)
        {
            if (input_list[i][index] == primaryDigit)
            {
                primary.Add(input_list[i]);
            } else
            {
                secondary.Add(input_list[i]);
            }
        }

        index++;

        if (keepMost)
        {
            if (secondary.Count > primary.Count)
            {
                finalBinaryNumber = FindLifeSupportRating(primaryDigit, secondary, index, keepMost);
            } else
            {
                finalBinaryNumber = FindLifeSupportRating(primaryDigit, primary, index, keepMost);
            }
        } else
        {
            if (secondary.Count < primary.Count)
            {
                finalBinaryNumber = FindLifeSupportRating(primaryDigit, secondary, index, keepMost);
            }
            else
            {
                finalBinaryNumber = FindLifeSupportRating(primaryDigit, primary, index, keepMost);
            }
        }

        return finalBinaryNumber;
    }

    void ReadTextFile(string file_path, IList<string> input_list)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_list.Add(line);
            }
        }
        input_stream.Close();
    }
}
