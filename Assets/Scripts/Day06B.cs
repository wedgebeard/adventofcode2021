using System.IO;
using UnityEngine;
using System;

public class Day06B : MonoBehaviour
{
    //answer: 1214653763 (wrong)
    //answer: 1695929023803 got it!

    public string file_path;
    private int daysToAge = 256;

    void Start()
    {
        int[] fishArray = ReadTextFile(file_path);
        //                                   0  1  2  3  4  5  6  7  8
        long[] gestationBuckets = new long[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };


        //    0  1  2  3  4  5  6  7  8

        //0   0  1  1  2  1  0  0  0  0  (5

        //1   1  1  2  1  0  0  0  0  0  (5
        //2   1  2  1  0  0  0  1  0  1  (6
        //3   2  1  0  0  0  1  1  1  1  (7
        //4   1  0  0  0  1  1  3  1  2  (9
        //5   0  0  0  1  1  3  2  2  1  (10
        //6   0  0  1  1  3  2  2  1  0  (10
        //7   0  1  1  3  2  2  1  0  0  (10
        //8   1  1  3  2  2  1  0  0  0  (10
        //9   1  3  2  2  1  0  1  0  1  (11
        //10  3  2  2  1  0  1  1  1  1  (12
        //11  2  2  1  0  1  1  4  1  3  (15
        //12
        //13
        //14

        for (int i = 0; i < fishArray.Length; i++)
        {
            gestationBuckets[fishArray[i]] += 1;
        }

        long totalFish = 0;
        int bucketCount = gestationBuckets.Length;

        for (int i = 0; i < daysToAge; i++)
        {
            long transferFish = gestationBuckets[0];
            for (int j = 0; j < bucketCount - 1; j++)
            {
                gestationBuckets[j] = gestationBuckets[j + 1];
            }
            gestationBuckets[8] = transferFish;
            gestationBuckets[6] += transferFish;

            totalFish = GetTotalFish(gestationBuckets);
            //Debug.Log("Total fish day " + (i+1) + ": " + totalFish);
        }

        //int totalFish = GetTotalFish(gestationBuckets);
        Debug.Log("done: total fish: " + totalFish);
    }

    private static long GetTotalFish(long[] gestationBuckets)
    {
        long totalFish = 0;
        for (int i = 0; i < gestationBuckets.Length; i++)
        {
            totalFish += gestationBuckets[i];
        }

        return totalFish;
    }

    int[] ReadTextFile(string file_path)
    {
        string stringInput = "";
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            stringInput = input_stream.ReadLine();
        }
        input_stream.Close();

        return Array.ConvertAll(stringInput.Split(','), int.Parse);
    }
}
