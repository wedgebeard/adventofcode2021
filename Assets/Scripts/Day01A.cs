using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class Day01A : MonoBehaviour
{
    public string file_path;

    void Start()
    {
        IList<int> input_array = new List<int>();
        ReadTextFile(file_path, input_array);
        int num_increases = Count_increases(input_array);
        Debug.Log("Day01A: times depth increased: " + num_increases);
    }

    void ReadTextFile(string file_path, IList<int> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if(input_stream == null)
        {
            Debug.Log("input stream is null");
        } else {
            while (!input_stream.EndOfStream)
            {
                int line = int.Parse(input_stream.ReadLine());
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }

    private int Count_increases(IList<int> input_array)
    {
        int increase_count = 0;
        int index = 1;
        while(index < input_array.Count)
        {
            if(input_array[index] > input_array[index - 1])
            {
                increase_count++;
            }
            index++;
        }
        return increase_count;
    }
}
