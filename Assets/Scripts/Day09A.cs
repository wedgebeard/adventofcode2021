using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Day09A : MonoBehaviour
{
    //answer: 1763 - too high
    //answer: 633

    public string file_path;

    void Start()
    {
        Debug.Log("Day 9 A");
        List<List<int>> map2d = ReadTextFile(file_path);
        List<int> lowPoints = FindLowPoints(map2d);
        int total = CalculateSumHeightMap(lowPoints);
        Debug.Log("SUM: " + total);
    }

    private int CalculateSumHeightMap(List<int> lowPoints)
    {
        int sum = 0;
        for (int i = 0; i < lowPoints.Count; i++)
        {
            sum += lowPoints[i] + 1;
        }
        return sum;
    }

    private List<int> FindLowPoints(List<List<int>> map2d)
    {
        List<int> lowPoints = new List<int>();

        for (int row = 0; row < map2d.Count; row++)
        {
            for (int col = 0; col < map2d[row].Count; col++)
            {
                if(CheckNorth(row, col, map2d) &&
                    CheckEast(row, col, map2d) &&
                    CheckSouth(row, col, map2d) &&
                    CheckWest(row, col, map2d))
                {
                    lowPoints.Add(map2d[row][col]);
                }
            }
        }

        return lowPoints;
    }

    private bool CheckWest(int row, int col, List<List<int>> map)
    {
        bool isLower = true;
        if (col > 0)
        {
            if (map[row][col] >= map[row][col - 1])
            {
                isLower = false;
            }
        }
        return isLower;
    }

    private bool CheckSouth(int row, int col, List<List<int>> map)
    {
        bool isLower = true;
        if (row < map.Count - 1)
        {
            if (map[row][col] >= map[row + 1][col])
            {
                isLower = false;
            }
        }
        return isLower;
    }

    private bool CheckEast(int row, int col, List<List<int>> map)
    {
        bool isLower = true;
        if (col < map[row].Count -1)
        {
            if(map[row][col] >= map[row][col + 1])
            {
                isLower = false;
            }
        }
        return isLower;
    }

    private bool CheckNorth(int row, int col, List<List<int>> map)
    {
        bool isLower = true;
        if (row > 0)
        {
            if (map[row][col] >= map[row - 1][col])
            {
                isLower = false;
            }
        }
        return isLower;
    }

    List<List<int>> ReadTextFile(string file_path)
    {
        List<List<int>> list2d = new List<List<int>>();

        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }

        while (!input_stream.EndOfStream)
        {
            string input = input_stream.ReadLine();
            char[] line = input.ToCharArray();
            list2d.Add(line.Select(x => int.Parse(x.ToString())).ToList());
        }

        input_stream.Close();

        return list2d;
    }
}
