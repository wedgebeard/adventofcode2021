﻿using System;

public class BingoCard
{
    public BingoCard(int rows, int columns) {
        _rows = rows;
        _columns = columns;
        CardEntries = new CardEntry[rows, columns];
    }

    public CardEntry[,] CardEntries { get; }
    private int _rows { get; }
    private int _columns { get; }
    public string winningNumber = "";
    public bool cardWon = false;

    public void UpdateEntry(int row, int column, string number, bool selected)
    {
        CardEntries[row, column] = new CardEntry(number, selected);
    }

    public void UpdateEntry(int row, int column, CardEntry entry)
    {
        UpdateEntry(row, column, entry.Number, entry.Selected);
    }

    public void MarkNumber(int row, int column)
    {
        CardEntries[row, column].Selected = true;
    }

    internal int UnmarkedSum()
    {
        int sum = 0;
        for (int row = 0; row < _rows; row++)
        {
            for (int col = 0; col < _columns; col++)
            {
                if (!CardEntries[row, col].Selected)
                {
                    sum += int.Parse(CardEntries[row, col].Number);
                }
            }
        }
        return sum;
    }

    public Tuple<int, int> Contains(string num)
    {
        for (int row = 0; row < _rows; row++)
        {
            for (int col = 0; col < _columns; col++)
            {
                if (CardEntries[row, col].Number == num)
                {
                    return new Tuple<int, int>(row, col);
                }
            }
        }
        return new Tuple<int, int>(-1, -1);
    }

    internal bool GetEntrySelected(int row, int column)
    {
        return CardEntries[row, column].Selected;
    }
}