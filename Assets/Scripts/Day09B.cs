using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Day09B : MonoBehaviour
{
    public string file_path;

    void Start()
    {
        Debug.Log("Day 9 B");
        List<List<Node>> map2d = ReadTextFile(file_path);
        List<int> basinAreasList = GetBasinAreasList(map2d);
        basinAreasList.Sort();
        int basinCount = basinAreasList.Count;
        int answer = basinAreasList[basinCount - 1] * basinAreasList[basinCount - 2] * basinAreasList[basinCount - 3];
        Debug.Log("Product: " + answer);
    }

    private List<int> GetBasinAreasList(List<List<Node>> map2d) // return 
    {
        List<int> basinAreasList = new List<int>();
        List<Node> currentBasinArea = new List<Node>();
        Queue<Node> currentQueue = new Queue<Node>();

        int rowCount = map2d.Count;
        int columnCount = map2d[0].Count;
        for (int row = 0; row < rowCount; row++)
        {
            for (int column = 0; column < columnCount; column++)
            {
                if (!map2d[row][column].InBasin && map2d[row][column].Height < 9)
                {
                    Node currNode = map2d[row][column];
                    currNode.InBasin = true;
                    currentBasinArea.Add(currNode);
                    currentQueue.Enqueue(currNode);

                    while(currentQueue.Count > 0)
                    {
                        currNode = currentQueue.Dequeue();
                        List<Node> neighbors = GetNeighborNodes(map2d, currNode);

                        foreach (Node node in neighbors)
                        {
                            currentQueue.Enqueue(node);
                            currentBasinArea.Add(node);
                        }
                    }
                }
                if (currentBasinArea.Count > 0)
                {
                    basinAreasList.Add(currentBasinArea.Count);
                }
                currentBasinArea.Clear();
            }
        }
        return basinAreasList;
    }

    private List<Node> GetNeighborNodes(List<List<Node>> map2d, Node currNode)
    {
        List<Node> neighborNodes = new List<Node>();
        int row = currNode.Position.Row;
        int column = currNode.Position.Col;

        Node northNode = CheckNorth(row, column, map2d);
        if (northNode != null) {
            northNode.InBasin = true;
            neighborNodes.Add(northNode); 
        }

        Node eastNode = CheckEast(row, column, map2d);
        if (eastNode != null) { 
            eastNode.InBasin = true;
            neighborNodes.Add(eastNode);
        }
        
        Node southNode = CheckSouth(row, column, map2d);
        if (southNode != null) {
            southNode.InBasin = true;
            neighborNodes.Add(southNode); 
        }
        
        Node westNode = CheckWest(row, column, map2d);
        if (westNode != null) {
            westNode.InBasin = true;
            neighborNodes.Add(westNode); 
        }
        
        return neighborNodes;
    }

    private Node CheckNorth(int row, int col, List<List<Node>> map)
    {
        Node neighborNode = null;
        if (row < 1) { return neighborNode; }

        Node nextNode = map[row - 1][col];
        neighborNode = IsValidNode(neighborNode, nextNode);

        return neighborNode;
    }

    private Node CheckEast(int row, int col, List<List<Node>> map)
    {
        Node neighborNode = null;

        if (col >= map[row].Count - 1) { return neighborNode; }

        Node nextNode = map[row][col+1];
        neighborNode = IsValidNode(neighborNode, nextNode);

        return neighborNode;
    }

    private Node CheckSouth(int row, int col, List<List<Node>> map)
    {
        Node neighborNode = null;

        if (row >= map.Count - 1) { return neighborNode; }

        Node nextNode = map[row+1][col];
        neighborNode = IsValidNode(neighborNode, nextNode);

        return neighborNode;
    }

    private Node CheckWest(int row, int col, List<List<Node>> map)
    {
        Node neighborNode = null;

        if (col < 1 ){ return neighborNode; }

        Node nextNode = map[row][col-1];
        neighborNode = IsValidNode(neighborNode, nextNode);

        return neighborNode;
    }

    private static Node IsValidNode(Node neighborNode, Node nextNode)
    {
        if (!nextNode.InBasin &&
           nextNode.Height < 9)
        {
            neighborNode = nextNode;
        }

        return neighborNode;
    }

    private int CalculateSumHeightMap(List<int> lowPoints)
    {
        int sum = 0;
        for (int i = 0; i < lowPoints.Count; i++)
        {
            sum += lowPoints[i] + 1;
        }
        return sum;
    }

    List<List<Node>> ReadTextFile(string file_path)
    {
        List<List<Node>> list2d = new List<List<Node>>();

        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }

        int currRow = 0;
        while (!input_stream.EndOfStream)
        {
            string input = input_stream.ReadLine();
            char[] line = input.ToCharArray();
            List<int> listLine = line.Select(x => int.Parse(x.ToString())).ToList();
            List<Node> nodeList = new List<Node>();
            for (int col = 0; col < listLine.Count; col++)
            {
                nodeList.Add(new Node(listLine[col], false, currRow, col));
            }
            list2d.Add(nodeList);
            currRow++;
        }

        input_stream.Close();

        return list2d;
    }
}
