using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day02A : MonoBehaviour
{
    public string file_path;
    private int horizontal = 0;
    private int depth = 0;

    void Start()
    {
        ReadTextFile(file_path);
        Debug.Log("Day02A: horizontal: " + horizontal + " | depth: " + depth + " | product: " + (horizontal * depth));
    }

    void ReadTextFile(string file_path)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string[] line = input_stream.ReadLine().Split(' ');
                if (line[0] == "forward")
                {
                    horizontal += int.Parse(line[1]);
                } else if (line[0] == "down")
                {
                    depth += int.Parse(line[1]);
                } else if (line[0] == "up")
                {
                    depth -= int.Parse(line[1]);
                }
            }
        }
        input_stream.Close();
    }
}
