using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day10B : MonoBehaviour
{
    public string file_path;
    //answer: 29184309 - too low
    //answer: 34879859 - too low
    //answer: 3969823589 - had to store in long
    void Start()
    {
        Debug.Log("Day 10 B");

        IList<string> input_array = new List<string>();
        ReadTextFile(file_path, input_array);

        Dictionary<char, char> chunkMap = new Dictionary<char, char>();
        chunkMap.Add('(', ')');
        chunkMap.Add('{', '}');
        chunkMap.Add('[', ']');
        chunkMap.Add('<', '>');

        Dictionary<char, char> reverseChunkMap = new Dictionary<char, char>();
        reverseChunkMap.Add(')', '(');
        reverseChunkMap.Add('}', '{');
        reverseChunkMap.Add(']', '[');
        reverseChunkMap.Add('>', '<');

        Dictionary<char, int> bracketValues = new Dictionary<char, int>();
        bracketValues.Add(')', 1);
        bracketValues.Add(']', 2);
        bracketValues.Add('}', 3);
        bracketValues.Add('>', 4);

        List<char> openers = new List<char>() { '(', '{', '[', '<' };

        Stack<char> chunkStack = new Stack<char>();

        Stack<Stack<char>> incompleteStack = new Stack<Stack<char>>();

        for (int lineIndex = 0; lineIndex < input_array.Count; lineIndex++)
        {
            bool corrupted = false;
            string currString = input_array[lineIndex];
            for (int charPosition = 0; charPosition < currString.Length; charPosition++)
            {
                char nextBracket = currString[charPosition];

                if (openers.Contains(nextBracket))
                {
                    chunkStack.Push(nextBracket);
                }
                else // you have a bracket at this point
                {
                    if (chunkStack.Count > 0 && chunkMap[chunkStack.Peek()] == nextBracket)
                    {
                        chunkStack.Pop();
                    }
                    else
                    {
                        corrupted = true;
                        chunkStack.Clear();
                        break;
                    }
                }
            }
            if (!corrupted)
            {
                incompleteStack.Push(new Stack<char>(chunkStack));
                chunkStack.Clear();
                corrupted = false;
            }
        }

        Stack<Stack<char>> completedStack = new Stack<Stack<char>>();

        while (incompleteStack.Count > 0)
        { 
            Stack<char> nextStack = incompleteStack.Pop();
            Stack<char> tempStack = new Stack<char>();
            while(nextStack.Count > 0) 
            {
                tempStack.Push(chunkMap[nextStack.Pop()]);
            }
            completedStack.Push(new Stack<char>(tempStack));
        }

        long runningTotal = 0;

        List<long> stackTotals = new List<long>();
        while(completedStack.Count > 0)
        {
            Stack<char> currBracketStack = completedStack.Pop();
            currBracketStack = ReverseStack(currBracketStack);
            while(currBracketStack.Count > 0)
            {
                runningTotal = (5 * runningTotal) + bracketValues[currBracketStack.Pop()];
            }
            stackTotals.Add(runningTotal);
            runningTotal = 0;
        }

        stackTotals.Sort();
        int midValue = stackTotals.Count / 2;

        Debug.Log("answer: middle stack total: " + stackTotals[midValue]);
    }

    private Stack<char> ReverseStack(Stack<char> currBracketStack)
    {
        Stack<char> tempStack = new Stack<char>();
        while(currBracketStack.Count > 0)
        {
            tempStack.Push(currBracketStack.Pop());
        }
        return tempStack;
    }

    void ReadTextFile(string file_path, IList<string> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }
}
