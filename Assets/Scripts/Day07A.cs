using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day07A : MonoBehaviour
{
    //answer: 355150
    public string file_path;

    void Start()
    {
        Debug.Log("Day 7 A:");
        int[] input_array = ReadTextFile(file_path);

        List<int> fuelCosts = GetFuelCosts(input_array);
        fuelCosts.Sort();
        Debug.Log("Day 07 fuel: " + fuelCosts[0]);
    }

    private List<int> GetFuelCosts(int[] input_array)
    {
        List<int> fuelCosts = new List<int>();

        for (int i = 0; i < input_array.Length; i++)
        {
            int accumulator = 0;
            for (int j = 0; j < input_array.Length; j++)
            {
                accumulator += Math.Max(input_array[i], input_array[j]) - Math.Min(input_array[i], input_array[j]);
            }
            fuelCosts.Add(accumulator);
        }

        return fuelCosts;
    }

    int[] ReadTextFile(string file_path)
    {
        string stringInput = "";
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            stringInput = input_stream.ReadLine();
        }
        input_stream.Close();

        return Array.ConvertAll(stringInput.Split(','), int.Parse);
    }
}
