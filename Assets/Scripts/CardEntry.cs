﻿using UnityEditor;
using UnityEngine;

public class CardEntry
{
    public CardEntry(string number, bool selected)
    {
        Number = number;
        Selected = selected;
    }

    public CardEntry(string number)
    {
        Number = number;
        Selected = false;
    }

    public string Number { get; }
    public bool Selected { get; set; }
}
