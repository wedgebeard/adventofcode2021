using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day11A : MonoBehaviour
{
    //Part 1: 1747
    //Part 2: 504 - too low
    //Part 2: 505 - was off by 1, derp, i didn't count 0

    [SerializeField]
    public string file_path;

    [SerializeField]
    int iterations = 2;

    void Start()
    {
        IList<string> input_array = new List<string>();
        ReadTextFile(file_path, input_array);

        int totalColumns = input_array[0].Length;
        int totalRows = input_array.Count;

        int[,] octopusArray = CreateOctopusArray(input_array, totalColumns, totalRows);
        Queue<Tuple<int,int>> glowQueue = new Queue<Tuple<int,int>>();

        int octopusGlowCount = 0;

        for (int i = 0; i < iterations; i++)
        {
            IncrementStep(ref octopusArray, totalRows, totalColumns, glowQueue);

            while(glowQueue.Count > 0)
            {
                Tuple<int, int> position = glowQueue.Dequeue();
                IncreaseNeighbors(ref octopusArray, position.Item1, position.Item2, totalRows, totalColumns, glowQueue);
            }
            //PrintArray("PRE-GLOW", octopusArray, totalColumns, totalRows);
            GlowOctopi(ref octopusArray, totalRows, totalColumns, ref octopusGlowCount);
            //PrintArray("POST-GLOW", octopusArray, totalColumns, totalRows);

            if (CheckFullFlash(octopusArray, totalRows, totalColumns))
            {
                Debug.Log("Full screen flash: " + i);
            }
        }

        Debug.Log("glowCount: " + octopusGlowCount);
    }
   
    private void IncrementStep(ref int[,] octopusArray, int totalRows, int totalColumns, Queue<Tuple<int, int>> glowQueue)
    {
        for (int row = 0; row < totalRows; row++)
        {
            for (int col = 0; col < totalColumns; col++)
            {
                octopusArray[row, col] += 1;
                if (octopusArray[row, col] > 9)
                {
                    glowQueue.Enqueue(new Tuple<int, int>(row, col));
                }
            }
        }
    }

    private void GlowOctopi(ref int[,] octopusArray, int totalRows, int totalColumns, ref int octopusGlowCount)
    {
        //iterate over array, get glows, reset glows to 0
        for (int row = 0; row < totalRows; row++)
        {
            for (int col = 0; col < totalColumns; col++)
            {
                if(octopusArray[row, col] > 9)
                {
                    octopusArray[row, col] = 0;
                    octopusGlowCount++;
                }
            }
        }
    }

    private void IncreaseNeighbors(ref int[,] octopusArray, int currRow, int currCol, int maxRows, int maxColumns, Queue<Tuple<int, int>> glowQueue)
    {
        if (currCol > 0)
        {
            //left
            UpdateNeighborAndCheckAddToQueue(currRow, currCol - 1, glowQueue, ref octopusArray);

            if (currRow > 0)
            {
                //top-left
                UpdateNeighborAndCheckAddToQueue(currRow - 1, currCol - 1, glowQueue, ref octopusArray);

            }
            if (currRow < maxRows - 1)
            {
                //bottom-left
                UpdateNeighborAndCheckAddToQueue(currRow + 1, currCol - 1, glowQueue, ref octopusArray);
            }
        }
        if (currCol < maxColumns - 1)
        {
            //right
            UpdateNeighborAndCheckAddToQueue(currRow, currCol + 1, glowQueue, ref octopusArray);

            if (currRow > 0)
            {
                //top-right
                UpdateNeighborAndCheckAddToQueue(currRow - 1, currCol + 1, glowQueue, ref octopusArray);

            }
            if (currRow < maxRows - 1)
            {
                //bottom-right
                UpdateNeighborAndCheckAddToQueue(currRow + 1, currCol + 1, glowQueue, ref octopusArray);

            }
        }
        if (currRow > 0)
        {
            //top
            UpdateNeighborAndCheckAddToQueue(currRow - 1, currCol, glowQueue, ref octopusArray);

        }
        if (currRow < maxRows - 1)
        {
            //bottom
            UpdateNeighborAndCheckAddToQueue(currRow + 1, currCol, glowQueue, ref octopusArray);

        }
    }

    private void UpdateNeighborAndCheckAddToQueue(int row, int col, Queue<Tuple<int, int>> glowQueue, ref int[,] octopusArray)
    {
        int prevTimer = octopusArray[row, col];
        octopusArray[row, col] += 1;
        int octoValue = octopusArray[row, col];
        if (octoValue > 9 && prevTimer == 9 && !glowQueue.Contains(new Tuple<int, int>(row, col)))
        {
            glowQueue.Enqueue(new Tuple<int, int>(row, col));
        }
    }

    private int[,] CreateOctopusArray(IList<string> input_array, int totalColumns, int totalRows)
    {
        int[,] tempArray = new int[totalRows, totalColumns];

        for (int row = 0; row < totalRows; row++)
        {
            for (int col = 0; col < totalColumns; col++)
            {
                int num = int.Parse(input_array[row].Substring(col, 1));
                tempArray[row, col] = num;
            }
        }

        return tempArray;
    }

    private bool CheckFullFlash(int[,] octopusArray, int totalRows, int totalColumns)
    {
        int count = 0;
        int target = totalColumns * totalRows;

        for (int row = 0; row < totalRows; row++)
        {
            for (int col = 0; col < totalColumns; col++)
            {
                if(octopusArray[row, col] == 0)
                {
                    count++;
                }
            }
        }

        return target == count;
    }

    private void PrintArray(string text, int[,] octopusArray, int width, int height)
    {
        Debug.Log(text);
        string assembled = "";
        for (int row = 0; row < height; row++)
        {
            for (int col = 0; col < width; col++)
            {
                assembled += octopusArray[row, col] + " | ";
            }
            assembled += "\n";
        }
        Debug.Log(assembled);
    }

    void ReadTextFile(string file_path, IList<string> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }
}
