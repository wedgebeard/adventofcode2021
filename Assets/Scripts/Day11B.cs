using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Day11B : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Queue<Tuple<int, int>> theQueue = new Queue<Tuple<int, int>>();
        theQueue.Enqueue(new Tuple<int, int>(0,0));
        theQueue.Enqueue(new Tuple<int, int>(1,2));
        theQueue.Enqueue(new Tuple<int, int>(1,1));
        theQueue.Enqueue(new Tuple<int, int>(2,1));

        if(theQueue.Contains(new Tuple<int, int>(1, 1)))
        {
            Debug.Log("already has it");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
