using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day05B : MonoBehaviour
{
    //answer 2: 22337 - too low
    //answer 2: 22338 - too low (not off by 1)
    //answer 2: 22364
    public string file_path;
    private IList<Tuple<int, int>> pointsList;
    Dictionary<Tuple<int, int>, int> positionsMap;

    void Start()
    {
        IList<string> input_array = new List<string>();
        ReadTextFile(file_path, input_array);

        pointsList = CreateLinesList(input_array);
        positionsMap = CreatePositionsMap(pointsList);

        int totalOverlappingPoints = CalculateTotalOveralappingPoints(positionsMap);
        Debug.Log("Day 05 B: Intersections: " + totalOverlappingPoints);
    }

    private int CalculateTotalOveralappingPoints(Dictionary<Tuple<int, int>, int> dict)
    {
        int total = 0;
        foreach (KeyValuePair<Tuple<int, int>, int> entry in dict)
        {
            if (entry.Value > 1)
            {
                total++;
            }
        }
        return total;
    }

    private Dictionary<Tuple<int, int>, int> CreatePositionsMap(IList<Tuple<int, int>> pointsList)
    {
        Dictionary<Tuple<int, int>, int> positionsMap = new Dictionary<Tuple<int, int>, int>();

        for (int i = 0; i < pointsList.Count; i += 2)
        {
            int x1 = pointsList[i].Item1;
            int y1 = pointsList[i].Item2;
            int x2 = pointsList[i + 1].Item1;
            int y2 = pointsList[i + 1].Item2;

            if (x1 == x2)
            {
                drawVerticalLine(x1, y1, y2, positionsMap);
            }
            else if (y1 == y2)
            {
                drawHorizontalLine(y1, x1, x2, positionsMap);
            }
            else if (x1 > x2 && y1 > y2) // x- y-
            {
                drawDiagonalLine(x1, x2, y1, y2, -1, -1, positionsMap);
            }
            else if (x1 > x2 && y1 < y2)// x- y+
            {
                drawDiagonalLine(x1, x2, y1, y2, -1, 1, positionsMap);
            }
            else if (x1 < x2 && y1 > y2)// x+ y-
            {
                drawDiagonalLine(x1, x2, y1, y2, 1, -1, positionsMap);
            }
            else if (x1 < x2 && y1 < y2) // x+ y+
            {
                drawDiagonalLine(x1, x2, y1, y2, 1, 1, positionsMap);
            }
        }

        return positionsMap;
    }

    private void drawDiagonalLine(int startX, int endX, int startY, int endY,
        int xChange, int yChange, Dictionary<Tuple<int, int>, int> positionsMap)
    {
        int totalLength = Math.Max(startX, endX) - Math.Min(startX, endX);

        for (int i = 0; i < totalLength + 1; i++)
        {
            Tuple<int, int> currentPoint = new Tuple<int, int>(startX, startY);

            if (positionsMap.ContainsKey(currentPoint))
            {
                positionsMap[currentPoint] = positionsMap[currentPoint] + 1;
            }
            else
            {
                positionsMap[currentPoint] = 1;
            }
            startX += xChange;
            startY += yChange;
        }
    }

    private void drawHorizontalLine(int y, int x1, int x2, Dictionary<Tuple<int, int>, int> positionsMap)
    {
        //y same
        for (int i = Math.Min(x1, x2); i < Math.Max(x1, x2) + 1; i++)
        {
            Tuple<int, int> currentPoint = new Tuple<int, int>(i, y);
            if (positionsMap.ContainsKey(currentPoint))
            {
                positionsMap[currentPoint] = positionsMap[currentPoint] + 1;
            }
            else
            {
                positionsMap[currentPoint] = 1;
            }
        }
    }

    private void drawVerticalLine(int x, int y1, int y2, Dictionary<Tuple<int, int>, int> positionsMap)
    {
        //x same
        for (int i = Math.Min(y1, y2); i < Math.Max(y1, y2) + 1; i++)
        {
            Tuple<int, int> currentPoint = new Tuple<int, int>(x, i);
            if (positionsMap.ContainsKey(currentPoint))
            {
                positionsMap[currentPoint] = positionsMap[currentPoint] + 1;
            }
            else
            {
                positionsMap[currentPoint] = 1;
            }
        }
    }

    private IList<Tuple<int, int>> CreateLinesList(IList<string> input_array)
    {
        IList<Tuple<int, int>> linesList = new List<Tuple<int, int>>();
        string[] stringSeparators = new string[] { " -> " };

        for (int i = 0; i < input_array.Count; i++)
        {
            string[] values = input_array[i].Split(stringSeparators, System.StringSplitOptions.None);
            string[] point1String = values[0].Split(',');
            string[] point2String = values[1].Split(',');

            linesList.Add(new Tuple<int, int>(int.Parse(point1String[0]), int.Parse(point1String[1])));
            linesList.Add(new Tuple<int, int>(int.Parse(point2String[0]), int.Parse(point2String[1])));
        }
        return linesList;
    }

    void ReadTextFile(string file_path, IList<string> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }
}
