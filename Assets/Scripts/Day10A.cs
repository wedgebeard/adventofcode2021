using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day10A : MonoBehaviour
{
    public string file_path;

    void Start()
    {
        //asnwer: 265527
        Debug.Log("Day 10 A");

        IList<string> input_array = new List<string>();
        ReadTextFile(file_path, input_array);
        Dictionary<char, char> chunkMap = new Dictionary<char, char>();
        chunkMap.Add('(', ')');
        chunkMap.Add('{', '}');
        chunkMap.Add('[', ']');
        chunkMap.Add('<', '>');

        Dictionary<char, int> bracketValues = new Dictionary<char, int>();
        bracketValues.Add(')', 3);
        bracketValues.Add(']', 57);
        bracketValues.Add('}', 1197);
        bracketValues.Add('>', 25137);

        List<char> openers = new List<char>() { '(', '{', '[', '<' };

        Stack<char> chunkStack = new Stack<char>();

        Stack<char> corrupted = new Stack<char>();

        for (int lineIndex = 0; lineIndex < input_array.Count; lineIndex++)
        {
            string currString = input_array[lineIndex];
            for (int charPosition = 0; charPosition < currString.Length; charPosition++)
            {
                char nextBracket = currString[charPosition];
                if(openers.Contains(nextBracket))
                {
                    chunkStack.Push(nextBracket);
                } 
                else
                {
                    if(chunkStack.Count > 0 && chunkMap[chunkStack.Peek()] == nextBracket)
                    {
                        chunkStack.Pop();
                    } 
                    else
                    {
                        corrupted.Push(nextBracket);
                        break;
                    }
                }
            }
        }

        int total = 0;
        while(corrupted.Count > 0)
        {
            total += bracketValues[corrupted.Pop()];
        }

        Debug.Log("corrupted " + total);
        Debug.Log("asdf");
    }

    void ReadTextFile(string file_path, IList<string> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }
}
