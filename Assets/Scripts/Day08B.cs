using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Day08B : MonoBehaviour
{
    public string file_path;

    // Digit    |   seg count
    //  1       |   2
    //  7       |   3
    //  4       |   4
    //  8       |   7
    //  3       |   5
    //  9       |   6
    //  5       |   5
    //  2       |   5
    //  0       |   6
    
    //  6       |   6

    //acedgfb: 8
    //cdfbe: 5
    //gcdfa: 2
    //fbcad: 3
    //dab: 7
    //cefabd: 9
    //cdfgeb: 6
    //eafb: 4
    //cagedb: 0
    //ab: 1

    void Start()
    {
        List<string[]> input_array = ReadTextFile(file_path);
        int totals = 0;
        for (int i = 0; i < input_array.Count; i++)
        {
            totals += GetSegmentIds(input_array[i]);
        }

        Debug.Log("Day 8 A: totals: " + totals);
    }

    private int GetSegmentIds(string[] input)
    {
        string[] data_array = new string[10];
        string[] four_outputs_array = new string[4];
        Array.Copy(input, data_array, 10);
        Array.Copy(input, 11, four_outputs_array, 0, 4);

        Dictionary<int, string> knownNumberMap = GetKnownNumbers(data_array);
        knownNumberMap.Add(3, GetNumber3(knownNumberMap, data_array)); // known good
        knownNumberMap.Add(9, GetNumber9(knownNumberMap, data_array)); // known good
        knownNumberMap.Add(5, GetNumber5(knownNumberMap, data_array)); // known good
        knownNumberMap.Add(2, GetNumber2(knownNumberMap, data_array)); // known good
        knownNumberMap.Add(0, GetNumber0(knownNumberMap, data_array)); // known good
        knownNumberMap.Add(6, GetNumber6(knownNumberMap, data_array)); // known good


        Dictionary<string, int> sortedNumberMap = new Dictionary<string, int>();
        foreach (KeyValuePair<int, string> segments in knownNumberMap)
        {
            char[] chars = segments.Value.ToCharArray();
            Array.Sort(chars);

            sortedNumberMap[new string(chars)] = segments.Key;
        }

        for (int i = 0; i < four_outputs_array.Length; i++)
        {
            char[] temp = four_outputs_array[i].ToCharArray();
            Array.Sort(temp);
            four_outputs_array[i] = new string(temp);
        }

        Debug.Log("asdf");
        string answer = sortedNumberMap[four_outputs_array[0]].ToString() +
            sortedNumberMap[four_outputs_array[1]].ToString() +
            sortedNumberMap[four_outputs_array[2]].ToString() +
            sortedNumberMap[four_outputs_array[3]].ToString();

        return int.Parse(answer);

    }

    private string GetNumber6(Dictionary<int, string> knownNumberMap, string[] data_array)
    {
        string stringRepresenting6 = "";
        for (int i = 0; i < data_array.Length; i++)
        {
            if(data_array[i].Length == 6 &&
                data_array[i] != knownNumberMap[9] &&
                data_array[i] != knownNumberMap[0])
            {
                stringRepresenting6 = data_array[i];
            }
        }
        return stringRepresenting6;
    }

    private string GetNumber0(Dictionary<int, string> knownNumberMap, string[] data_array)
    {
        string stringRepresenting0 = "";
        char[] seven = knownNumberMap[7].ToCharArray();
        for (int i = 0; i < data_array.Length; i++)
        {
            if (data_array[i].Length == 6)
            {
                char[] possibleZero = data_array[i].ToCharArray();
                if (possibleZero.Contains(seven[0]) &&
                    possibleZero.Contains(seven[1]) &&
                    possibleZero.Contains(seven[2]) &&
                    data_array[i] != knownNumberMap[9])
                {
                    stringRepresenting0 = data_array[i];
                }
            }
        }
        return stringRepresenting0;
    }

    private string GetNumber2(Dictionary<int, string> knownNumberMap, string[] data_array)
    {
        string number2 = "";
        for (int i = 0; i < data_array.Length; i++)
        {
            if(data_array[i].Length == 5 && 
                data_array[i] != knownNumberMap[5] &&
                data_array[i] != knownNumberMap[3])
            {
                number2 = data_array[i];
            }
        }
        return number2;
    }

    private string GetNumber5(Dictionary<int, string> knownNumberMap, string[] data_array)
    {
        string stringRepresenting5 = "";

        char[] nine = knownNumberMap[9].ToCharArray();
        for (int i = 0; i < data_array.Length; i++)
        {
            if (data_array[i].Length == 5)
            {
                char[] possibleFive = data_array[i].ToCharArray();
                if (nine.Contains(possibleFive[0]) &&
                    nine.Contains(possibleFive[1]) &&
                    nine.Contains(possibleFive[2]) &&
                    nine.Contains(possibleFive[3]) &&
                    nine.Contains(possibleFive[4]) &&
                    data_array[i] != knownNumberMap[3])
                {
                    stringRepresenting5 = data_array[i];
                }
            }
        }
        return stringRepresenting5;
    }

    private string GetNumber9(Dictionary<int, string> knownNumberMap, string[] data_array)
    {
        string stringRepresenting9 = "";
        for (int i = 0; i < data_array.Length; i++)
        {
            char[] three = knownNumberMap[3].ToCharArray();
            if (data_array[i].Length == 6)
            {
                stringRepresenting9 = data_array[i];
                char[] nine = stringRepresenting9.ToCharArray();
                IEnumerable<char> diff = nine.Except(three);

                int count = 0;
                foreach (char item in diff)
                {
                    count++;
                }

                if (count == 1)
                {
                    return stringRepresenting9;
                }
            }
        }
        return stringRepresenting9;
    }

    private string GetNumber3(Dictionary<int, string> knownNumberMap, string[] data_array)
    {
        string stringRepresenting3 = "";

        char[] seven = knownNumberMap[7].ToCharArray();
        for (int i = 0; i < data_array.Length; i++)
        {
            if(data_array[i].Length == 5)
            {
                char[] possibleThree = data_array[i].ToCharArray();
                if (possibleThree.Contains(seven[0]) &&
                    possibleThree.Contains(seven[1]) &&
                    possibleThree.Contains(seven[2]))
                {
                    stringRepresenting3 = data_array[i];
                }
            }
        }

        return stringRepresenting3;
    }

    private char GetSegment6(string four, char seg0, string[] data_array)
    {
        four.Append(seg0);
        return 's';
    }

    private char GetSegment0(string[] data_array, Dictionary<int, string> known_dict)
    {
        char[] seven = known_dict[7].ToCharArray();
        char[] one = known_dict[1].ToCharArray();

        IEnumerable<char> diff = seven.Except(one);
        char result = 'x';
        foreach (char letter in diff)
        {
            result = letter;
        }
        return result;
    }

    private Dictionary<int, string> GetKnownNumbers(string[] data_array)
    {
        Dictionary<int, string> knownDict = new Dictionary<int, string>();
        for (int i = 0; i < data_array.Length; i++)
        {
            if(data_array[i].Length == 2)
            {
                knownDict.Add(1, data_array[i]);
            } 
            else if (data_array[i].Length == 3) 
            { 
                knownDict.Add(7, data_array[i]);
            } 
            else if (data_array[i].Length == 4)
            {
                knownDict.Add(4, data_array[i]);
            } 
            else if (data_array[i].Length == 7) 
            { 
                knownDict.Add(8, data_array[i]);
            }
        }
        return knownDict;
    }

    

    private int GetUniqueSegments(List<string[]> input)
    {
        Dictionary<string, int> uniqueCountDict = new Dictionary<string, int>();
        for (int i = 0; i < input.Count; i++)
        {
            for (int output = 11; output < 15; output++)
            {
                if (uniqueCountDict.ContainsKey(input[i][output]))
                {
                    uniqueCountDict[input[i][output]]++;
                }
                else
                {
                    uniqueCountDict.Add(input[i][output], 1);
                }
            }
        }
        int[] lengths = { 2, 3, 4, 7 };

        int uniqueCount = GetUniquedCountForLengths(lengths, uniqueCountDict);
        int totalCount = GetTotalCountForLengths(lengths, uniqueCountDict);

        return totalCount;
    }

    private int GetTotalCountForLengths(int[] lengths, Dictionary<string, int> dict)
    {
        int uniqueLengthCount = 0;

        foreach (KeyValuePair<string, int> entry in dict)
        {
            for (int index = 0; index < lengths.Length; index++)
            {
                if (entry.Key.Length == lengths[index])
                {
                    uniqueLengthCount += entry.Value;
                }
            }
        }

        return uniqueLengthCount;
    }

    private int GetUniquedCountForLengths(int[] lengths, Dictionary<string, int> dict)
    {
        int uniqueLengthCount = 0;

        foreach (KeyValuePair<string, int> entry in dict)
        {
            for (int index = lengths[0]; index < lengths.Length; index++)
            {
                if (entry.Key.Length == lengths[index])
                {
                    uniqueLengthCount++;
                }
            }
        }

        return uniqueLengthCount;
    }

    List<string[]> ReadTextFile(string file_path)
    {
        List<string[]> input = new List<string[]>();

        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }

        while (!input_stream.EndOfStream)
        {
            input.Add(input_stream.ReadLine().Split(' '));

        }

        input_stream.Close();

        return input;
    }
}
