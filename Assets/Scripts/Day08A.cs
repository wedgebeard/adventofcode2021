using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day08A : MonoBehaviour
{
    //145 - too low (uniquely occuring 1, 4, 7, 8 (this is wrong, i was using the number representation instead of segment counts)
    //303 - got it
    public string file_path;

    void Start()
    {
        List<string[]> input_array = ReadTextFile(file_path);

        int uniqueSegments = GetUniqueSegments(input_array);
        Debug.Log("Day 8 A: unique count: " + uniqueSegments);
    }

    private int GetUniqueSegments(List<string[]> input)
    {
        Dictionary<string, int> uniqueCountDict = new Dictionary<string, int>();
        for (int i = 0; i < input.Count; i++)
        {
            for (int output = 11; output < 15; output++)
            {
                if (uniqueCountDict.ContainsKey(input[i][output]))
                {
                    uniqueCountDict[input[i][output]]++;
                } else
                {
                    uniqueCountDict.Add(input[i][output], 1);
                }
            }
        }
        int[] lengths = { 2, 3, 4, 7 };

        int uniqueCount = GetUniquedCountForLengths(lengths, uniqueCountDict);
        int totalCount = GetTotalCountForLengths(lengths, uniqueCountDict);
        
        return totalCount;
    }

    private int GetTotalCountForLengths(int[] lengths, Dictionary<string, int> dict)
    {
        int uniqueLengthCount = 0;

        foreach (KeyValuePair<string, int> entry in dict)
        {
            for (int index = 0; index < lengths.Length; index++)
            {
                if (entry.Key.Length == lengths[index])
                {
                    uniqueLengthCount += entry.Value;
                }
            }
        }

        return uniqueLengthCount;
    }

    private int GetUniquedCountForLengths(int[] lengths, Dictionary<string, int> dict)
    {
        int uniqueLengthCount = 0;

        foreach (KeyValuePair<string, int> entry in dict)
        {
            for (int index = lengths[0]; index < lengths.Length; index++)
            {
                if(entry.Key.Length == lengths[index])
                {
                    uniqueLengthCount++;
                }
            }
        }

        return uniqueLengthCount;
    }

    List<string[]> ReadTextFile(string file_path)
    {
        List<string[]> input = new List<string[]>();

        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }

        while (!input_stream.EndOfStream)
        {           
            input.Add(input_stream.ReadLine().Split(' '));

        }
        
        input_stream.Close();

        return input;
    }
}
