using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day04A : MonoBehaviour
{
    //answer: 35711
    public string file_path;
    IList<string> input_array = new List<string>();
    static int bingoCardStartingIndex = 2;

    static int cardColumns = 5;
    static int cardRows = 5;

    void Start()
    {
        ReadTextFile(file_path, input_array);

        string[] lottoNumbers = input_array[0].Split(',');

        IList<BingoCard> bingoCards = GenerateBingoCards(input_array, cardRows, cardColumns, bingoCardStartingIndex);

        BingoCard winningCard =  FindWinner(bingoCards, lottoNumbers);
        int unmarkedSum = winningCard.UnmarkedSum();

        Debug.Log("Day 04A: score: " + (unmarkedSum * int.Parse(winningCard.winningNumber)));
    }

    void ReadTextFile(string file_path, IList<string> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }

    private IList<BingoCard> GenerateBingoCards(IList<string> input_array, int cardRows, int cardColumns, int bingoCardStartingIndex)
    {
        IList<BingoCard> bingoCards = new List<BingoCard>();
        BingoCard bingoCard = new BingoCard(cardRows, cardColumns);

        int row = 0;
        int inputIndex = bingoCardStartingIndex;

        while(inputIndex < input_array.Count)
        {
            string[] line = input_array[inputIndex].Split(' ');
            line = RemoveSpaces(line, cardColumns);
            for (int column = 0; column < line.Length; column++)
            {
                bingoCard.UpdateEntry(row, column, new CardEntry(line[column]));
            }
            inputIndex++;
            row++;
            
            if(row ==  cardRows)
            {
                row = 0;
                inputIndex++;
                bingoCards.Add(bingoCard);
                bingoCard = new BingoCard(cardRows, cardColumns);
            }
        }

        return bingoCards;
    }

    private string[] RemoveSpaces(string[] line, int columns)
    {
        string[] newLine = new string[columns];
        int index = 0;
        foreach (string item in line)
        {
            if (item != "")
            {
                newLine[index] = item;
                index++;
            }
        }
        return newLine;
    }

    private BingoCard FindWinner(IList<BingoCard> bingoCards, string[] lottoNumbers)
    {
        for (int lottoNumberIndex = 0; lottoNumberIndex < lottoNumbers.Length; lottoNumberIndex++)
        {
            for (int currCardIndex = 0; currCardIndex < bingoCards.Count; currCardIndex++)
            {
                UpdateBingoCards(lottoNumbers[lottoNumberIndex], bingoCards);
                BingoCard winningCard = CheckForWinner(bingoCards);
                if(winningCard != null)
                {
                    winningCard.winningNumber = lottoNumbers[lottoNumberIndex];
                    return winningCard;
                }
            }
        }
        return null;
    }

    private void UpdateBingoCards(string num, IList<BingoCard> bingoCards)
    {
        for (int cardIndex = 0; cardIndex < bingoCards.Count; cardIndex++)
        {
            Tuple<int, int> position = bingoCards[cardIndex].Contains(num);

            if (position.Item1 != -1)
            {
                bingoCards[cardIndex].MarkNumber(position.Item1, position.Item2);
            }
        }
    }
    
    private BingoCard CheckForWinner(IList<BingoCard> bingoCards)
    {
        for (int currentCard = 0; currentCard < bingoCards.Count; currentCard++)
        {
            int rowMarkedOffCount = 0;
            int columnMarkedOffCount = 0;

            //check rows for win
            for (int row = 0; row < cardRows; row++)
            {
                for (int column = 0; column < cardColumns; column++)
                {
                    if (bingoCards[currentCard].GetEntrySelected(row, column))
                    {
                        rowMarkedOffCount++;
                    }
                }
                if(rowMarkedOffCount == cardColumns)
                {
                    return bingoCards[currentCard];
                } else
                {
                    rowMarkedOffCount = 0;
                }
            }

            //check columns for win
            for (int column = 0; column < cardColumns; column++)
            {
                for (int row = 0; row < cardRows; row++)
                {
                    if (bingoCards[currentCard].GetEntrySelected(row, column))
                    {
                        columnMarkedOffCount++;
                    }
                }
                if(columnMarkedOffCount == cardColumns)
                {
                    return bingoCards[currentCard];
                } else
                {
                    columnMarkedOffCount = 0;
                }
            }
        }
        return null;
    }
}
