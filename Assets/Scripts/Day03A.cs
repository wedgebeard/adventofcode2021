using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day03A : MonoBehaviour
{
    public string file_path;
    //correct answer is: 1997414
    void Start()
    {
        IList<string> input_list = new List<string>();
        ReadTextFile(file_path, input_list);
        int totalPositions = input_list[0].ToString().Length;
        int[] ones = new int[totalPositions];
        int[] zeroes = new int[totalPositions];
        AccumulateCounts(ref ones, ref zeroes, input_list, totalPositions);
        long epsilon = ConvertBinaryToInt(ones, zeroes, totalPositions);
        long gamma = ConvertBinaryToInt(zeroes, ones, totalPositions);
        Debug.Log("power consumption: " + epsilon * gamma);
    }

    private long ConvertBinaryToInt(int[] primary, int[] secondary, int totalPositions)
    {
        long decimalNumber = 0;

        for (int i = totalPositions - 1; i > -1; i--)
        {
            if (primary[i] > secondary[i])
            {
                decimalNumber += (long)(Math.Pow(2, (totalPositions - i - 1)));
            }
        }
        return decimalNumber;
    }

    private void AccumulateCounts(ref int[] ones, ref int[] zeroes, IList<string> input_list, int totalPositions)
    {
        foreach (string line in input_list)
        {
            for (int i = 0; i < totalPositions; i++)
            {
                if (line[i] == '1')
                {
                    ones[i] += 1;
                }
                else if (line[i] == '0')
                {
                    zeroes[i] += 1;
                }
            }
        }
    }

    void ReadTextFile(string file_path, IList<string> input_array)
    {
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            while (!input_stream.EndOfStream)
            {
                string line = input_stream.ReadLine();
                input_array.Add(line);
            }
        }
        input_stream.Close();
    }
}
