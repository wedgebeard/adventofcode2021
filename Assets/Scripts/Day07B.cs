using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Day07B : MonoBehaviour
{
    public string file_path;

    void Start()
    {
        //answer  98277441 too low
        //answer 220730835 too high - i left hard coded values for smallest and largest - doofus..
        //answer 98368490 - got it

        int[] input_array = ReadTextFile(file_path);
        Array.Sort<int>(input_array);
        int largest = input_array[input_array.Length - 1];
        int smallest = input_array[0];
        List<double> fuelCosts = GetFuelCosts(input_array, largest, smallest);
        fuelCosts.Sort();
        Debug.Log("Day 07 fuel: " + fuelCosts[0]);
    }

    private List<double> GetFuelCosts(int[] input_array, int largest, int smallest)
    {
        List<double> fuelCosts = new List<double>();

        for (int i = smallest; i <= largest; i++)
        {
            double accumulator = 0;
            for (int j = 0; j < input_array.Length; j++)
            {
                long distance = Math.Max(input_array[j], i) - Math.Min(input_array[j], i);
                double newFuelCost = (((double)distance + 1) * ((double)distance / 2));
                accumulator += newFuelCost;
            }
            fuelCosts.Add(accumulator);
        }

        return fuelCosts;
    }

    int[] ReadTextFile(string file_path)
    {
        string stringInput = "";
        StreamReader input_stream = new StreamReader(file_path);
        if (input_stream == null)
        {
            Debug.Log("input stream is null");
        }
        else
        {
            stringInput = input_stream.ReadLine();
        }
        input_stream.Close();

        return Array.ConvertAll(stringInput.Split(','), int.Parse);
    }
}
